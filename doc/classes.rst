Configuration
=============

These classes are used to configure the individual parts of buildbot.

.. automodule:: config.builders
   :members:

.. automodule:: config.buildsteps
   :members:

.. automodule:: config.changesource
   :members:

.. automodule:: config.database
   :members:

.. automodule:: config.gerrit_configuration
   :members:

.. automodule:: config.network
   :members:

.. automodule:: config.schedulers
   :members:

.. automodule:: config.slaves
   :members:

.. automodule:: config.status
   :members:
