.. Qt buildbot documentation master file, created by
   sphinx-quickstart on Tue Aug 26 11:07:19 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Qt buildbot's documentation!
=======================================

This is a simple guide to Qt's buildbot setup.

.. toctree::
   :maxdepth: 2

   classes.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

