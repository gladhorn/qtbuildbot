from buildbot.process.factory import BuildFactory
from buildbot.process import properties
from buildbot.process.properties import Interpolate
from buildbot.steps import vstudio
from buildbot.steps.shell import Compile
from buildbot.steps.shell import ShellCommand
from buildbot.steps.slave import MakeDirectory
from buildbot.steps.slave import RemoveDirectory
from buildbot.steps.source.git import Git
from buildbot.steps.source.gerrit import Gerrit
from buildbot.steps.transfer import FileUpload
from buildbot.steps.transfer import FileDownload
from buildbot.steps.trigger import Trigger
from buildbot.status.builder import SUCCESS, SKIPPED

class QtMetaBuildstepsFactory(BuildFactory):
    """ Factory that creates metastep triggering all possible
    builders for a given module. When all steps are finished
    there is a quarantie that the whole testing process was
    finished """

    def __init__(self, module):
        BuildFactory.__init__(self)
        self.addStep(ShellCommand(
            command=['echo', 'Trigger all dependent builds'],
            description='Starting all builds',
        ))
        self.addStep(
            Trigger(
                schedulerNames=[module + ' custom configuration'],
                waitForFinish=True
            )
        )
        self.addStep(ShellCommand(
            command=['echo', 'All builds were finished'],
            description='Finishing all builds',
        ))


class QtBuildstepsFactory(BuildFactory):
    """ Factory that returns the steps needed for a Build of a Qt module """
    def __init__(self, platform, module, archive = False):
        BuildFactory.__init__(self)
        self.platform = platform
        self.module = module
        self.repo_dir = module.split('/')[-1] # make qtbase out of qt/qtbase

        self.check_tools()

        # for non-gerrit triggers:
        #self.addStep(Git(repourl="git://gitorious.org/qt/qtbase.git",
                         #branch='5.4',
                         #workdir = self.repo_dir,))

        self.checkout()
        self.addStep(ShellCommand(
            command=['git', 'show', 'HEAD'],
            description='Showing the last change',
            workdir = self.repo_dir,
        ))


        #start with a clean build dir
        self.addStep(RemoveDirectory(name = "Remove build directory", description="Removing the old build directory", dir="build"))
        self.addStep(RemoveDirectory(name = "Remove installation directory", description="Removing the old installation directory", dir="install"))
        self.addStep(MakeDirectory(name = "Make new build directory", description="Making fresh build directory", dir="build"))

        if self.repo_dir == 'qtbase':
            self.qt_configure()
        else:
            self.unzip()
            self.qmake()

        self.make()
        self.make(['install'])

        if archive:
            self.qt_archive()

        self.addStep(RemoveDirectory(description="Removing the current build directory", dir="build"))
        self.addStep(RemoveDirectory(description="Removing the current installation directory", dir="install"))


    def check_tools(self):
        pass

    def checkout(self):
        self.addStep(Gerrit(
            repourl = 'ssh://frederik@codereview.qt-project.org:29418/' + self.module,
            workdir = self.repo_dir,
            mode = 'full',
            retry=(30, 4), # if failed retry 4 times after 30 seconds delay
            retryFetch = True
        ))


    def qt_configure(self, extra_args = []):
        command_list = None

        configure_cmd = '../qtbase/configure'
        if self.platform == 'win':
            configure_cmd += '.bat'

        # FIXME: this should be done through properties: http://docs.buildbot.net/current/manual/cfg-properties.html
        # that would mean we can simply change builders or create builders with a different set of options
        command_list = [configure_cmd, "-confirm-license", "-opensource", "-debug", "-nomake", "examples"]
        install_dir = '../install'
        command_list += ['-prefix', install_dir]
        command_list += extra_args
        conf = ShellCommand(name = "configure",
            command = command_list,
            haltOnFailure = True,
            description = "Configuring the project (configure: " + ' '.join(command_list) + ')'
        )
        self.addStep(conf)

    def qmake(self):
        qmake_cmd = ['../install/bin/qmake', '../' + self.repo_dir]
        cmd = ShellCommand(
            name = 'qmake',
            command = qmake_cmd,
            haltOnFailure = True,
            description = 'Running qmake',
        )
        self.addStep(cmd)

    def make(self, make_args = []):
        # note: we may want to use WarningCountingShellCommand at some point
        make = None
        if self.platform == 'win':
            # maybe use vstudio.VS2013
            make = ShellCommand(
                name = 'jom',
                # FXIME
                command = ['c:\\qt\\qtcreator-3.0.1\\bin\\jom',] + make_args,
                haltOnFailure = True,
                description = 'Runing jom',
            )
        else:
            make = Compile(name = 'make',
                command = ['make', ] + make_args, # '--silent'
                haltOnFailure = True,
                description = 'Running make',
                warningPattern="warning: ",
            )
        self.addStep(make)

    def qt_archive(self):
        self.addStep(
            ShellCommand(
                name = 'zip',
                command = ['zip', '-ry1', Interpolate(self.repo_dir + '-%(src::branch)s-install.zip'), 'install'],
                description = "Archiving artifacts",
                workdir = '.',
            )
        )
        self.addStep(
            FileUpload(
                slavesrc = Interpolate(self.repo_dir + '-%(src::branch)s-install.zip'),
                masterdest = Interpolate('~/data/buildbot/' + self.repo_dir + '-%(src::branch)s-install.zip'),
                description = "Uploading artifacts",
                workdir = '.',
            )
        )

    # FIXME work with more than qtbase
    def unzip(self):
        @properties.renderer
        def get_master_filename(props):
            sourceStamp = props.getBuild().getSourceStamp().asDict()
            branch = sourceStamp['branch']
            if branch.startswith('refs'):
                # we get refs/staging/5.4 for updated patches
                branch = branch.split('/')[-1]
            else: # we get 5.3/93570 for new changes
                branch = branch.split('/')[0]

            if len(branch) == 0:
                branch = 'dev' # fallback
            return '~/data/buildbot/qtbase-' + branch + '-install.zip'

        self.addStep(RemoveDirectory(description="rm install", dir="install"))
        file_download = FileDownload(
                name = "download baseline dependency",
                mastersrc = get_master_filename,
                slavedest = 'qtbase-install.zip',
                workdir = '.',
                description = "Downloading artifacts",
                warnOnFailure = True,
                flunkOnWarnings = False,
        )
        self.addStep(file_download)
        def checkIfBaselineDownloadFailed(step):
            stepStatus = step.build.getStatus().getSteps()
            download_status = filter(lambda s: file_download.name == s.getName(), stepStatus)[0]
            download_status = download_status.getResults()[0]
            return download_status != SUCCESS

        def hideIfSkipped(results, step):
            return results == SKIPPED

        self.addStep(
            Trigger(
                name = "create baseline artifact",
                doStepIf = checkIfBaselineDownloadFailed,
                schedulerNames=[self.module + ' triggerable updater',],
                waitForFinish=True,
                hideStepIf=hideIfSkipped
            )
        )
        self.addStep(
            FileDownload(
                name = "Retry download baseline dependency",
                mastersrc = get_master_filename,
                slavedest = 'qtbase-install.zip',
                workdir = '.',
                #description = "Downloading fresh artifacts", # TODO there is a bug in buildbot that tries to concatenate this with a list
                doStepIf = checkIfBaselineDownloadFailed,
                hideStepIf=hideIfSkipped
            )
        )
        self.addStep(
            ShellCommand(
                name = 'unzip',
                command = ['unzip', 'qtbase-install.zip'],
                workdir = '.',
                description = "Unziping artifacts",
            )
        )
