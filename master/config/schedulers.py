####### SCHEDULERS

from buildbot.schedulers.forcesched import ForceScheduler
from buildbot.schedulers.basic import AnyBranchScheduler
from buildbot.schedulers.triggerable import Triggerable
from buildbot.changes.filter import ChangeFilter

from qt_global import QtGlobal

def get_schedulers():
    """ Configure the Schedulers, which decide how to react to incoming changes.
    """

    schedulers = []

    for module in QtGlobal.modules:
        # react to gerrit changes
        repo_dir = module.split('/')[-1]

        gerrit_scheduler = AnyBranchScheduler(
            name = module + ' gerrit',
            change_filter = ChangeFilter(project = module),
            treeStableTimer = None,
            builderNames = [repo_dir + ' all']
        )

        triggerable_scheduler = Triggerable(
            name = repo_dir + ' custom configuration',
            builderNames = [
                repo_dir + '-ubuntu-14.04-x64',
                repo_dir + '-win8.1-x64-vs2013',
            ]
        )

        # react to updates in the repo (after successful integration)
        poll_filter = ChangeFilter(
            project = module,
            category = 'poll',
        )

        # TODO in long term we want to kill this one and relay on the gerrit
        # sending the right event (ref-update)
        git_scheduler = AnyBranchScheduler(
            name = module + 'updater',
            change_filter = poll_filter,
            treeStableTimer = None,
            builderNames = [ 'update-' + repo_dir + '-ubuntu-14.04-x64', 'update-' + repo_dir + '-win8.1-x64-vs2013', ]
        )

        updater_triggerable_scheduler = Triggerable(
            name = module + ' triggerable updater',
            builderNames = [ 'update-' + repo_dir + '-ubuntu-14.04-x64', ]
        )


        force_scheduler = ForceScheduler(
            name = 'force ' + module,
            builderNames = [ repo_dir + '-ubuntu-14.04-x64', repo_dir + '-win8.1-x64-vs2013',
                            'update-' + repo_dir + '-ubuntu-14.04-x64', 'update-' + repo_dir + '-win8.1-x64-vs2013', ]
        )

        schedulers += [gerrit_scheduler, triggerable_scheduler, git_scheduler, updater_triggerable_scheduler, force_scheduler]

    return schedulers
