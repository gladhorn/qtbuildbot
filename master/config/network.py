

def get_slave_portnum():
    """ 'protocols' contains information about protocols which master will use for
    communicating with slaves.
    You must define at least 'port' option that slaves could connect to your master
    with this protocol.
    'port' must match the value configured into the buildslaves (with their
    --master option)
    """
    return 9989

def get_buildbotURL():
    """ the 'buildbotURL' string should point to the location where the buildbot's
    internal web server (usually the html.WebStatus page) is visible. This
    typically uses the port number set in the Waterfall 'status' entry, but
    with an externally-visible host name which the buildbot cannot figure out
    without some help.
    """
    return 'http://localhost:8010/'
