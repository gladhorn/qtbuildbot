""" basic helpers """

import os
import subprocess

def create_archive(output_dir, file_name, base_dir, files):
    """ Create an archive with name file_name in the output_dir.
    files are subdirectories or files in base_dir.
    """

    print('Hello!')
    print(os.getcwd())

    archive_file = os.path.join(output_dir, file_name)
    old_dir = os.getcwd()
    os.chdir(base_dir)
    command = ['zip', '-yr', archive_file] + files
    subprocess.check_call(command)
    os.chdir(old_dir)
