
def get_db():
    """ The BuildBot database configuration.
    For experiments it's easiest to just use sqlite.
    """
    db_config = {
        'db_url' : 'sqlite:///state.sqlite',
    }
    return db_config
