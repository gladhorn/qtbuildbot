####### CHANGESOURCES

from buildbot.changes.gitpoller import GitPoller
from buildbot.changes.gerritchangesource import GerritChangeSource

from gerrit_configuration import QtGerrit
from qt_global import QtGlobal

def get_sources():
    """ the 'change_source' setting tells the buildmaster how it should find out
    about source code changes.
    """
    sources = []


    for module in QtGlobal.modules:
        sources.append(
            GitPoller(
                'git://gitorious.org/' + module + '.git',
                branches = True, # all branches
                pollinterval = 300, # five minutes
                category = 'poll',
            )
        )

    # listen to gerrit events: every new patch or update will
    # trigger a change event which we can later process
    sources.append(
        GerritChangeSource(
            gerritserver = QtGerrit.gerritserver,
            username = QtGerrit.username,
            gerritport = QtGerrit.gerritport,
            identity_file = QtGerrit.identity_file,
            handled_events=("patchset-created",) # ref-updated is sent when staging or patches are merged, nothing to test.
        )
    )

    return sources
