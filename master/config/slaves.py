

####### BUILDSLAVES

# The 'slaves' list defines the set of recognized buildslaves. Each element is
# a BuildSlave object, specifying a unique slave name and password.  The same
# slave name and password must be configured on the slave.


# These are all the basic operating systems which we have available
operating_systems = [
    'linux-Ubuntu_12.04_x64',
    'linux-Ubuntu_11.10_x86',
    'linux-Ubuntu_11.10_x64',
    'linux-OpenSuSE_13.1_x64',
    'linux-RHEL65_x64',
    'macx-OSX_10.7',
    'macx-OSX_10.8',
    'macx-OSX_10.9',
    'win32-Windows_7',
    'win64-Windows_81',
]

from buildbot.buildslave import BuildSlave

def get_slaves():
    """ A list of all build slaves.
    A slave corresponds to a real machine or virtual machine.
    """
    slaves = []

    # FIXME remove the local dummy slave
    slaves.append(BuildSlave('localslave', 'pass'))
    slaves.append(BuildSlave('win8.1', 'pass'))

    # generate a whole pile of slaves, fixed number for now
    #for os in operating_systems:
        #for i in range(3):
            #slaves.append(
                #BuildSlave(os + '_' + str(i), 'pass',
                    #notify_on_missing="frederik.gladhorn@digia.com"
                #)
            #)
    return slaves
