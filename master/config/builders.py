####### BUILDERS


from buildbot.config import BuilderConfig
from buildsteps import QtBuildstepsFactory, QtMetaBuildstepsFactory

from qt_global import QtGlobal

configs = {}

for module in QtGlobal.modules:
    repo = module.split('/')[-1]
    configs[repo + '-ubuntu-14.04-x64'] = {
            'slaves': ['localslave', ],
            'platform': 'linux',
            'module': module,
            'category': repo,
        }

    configs[repo + '-win8.1-x64-vs2013'] = {
            'slaves': ['win8.1', ],
            'platform': 'win',
            'module': module,
            'category': repo,
        }


def get_builders():
    """ Returns a list of all builders used.
    The 'builders' list defines the Builders, which tell Buildbot how to perform a build:
    what steps, and which slaves can execute them.  Note that any particular build will
    only take place on one slave.
    """
    builders = []

    for module in QtGlobal.modules:
        module = module.split('/')[-1]
        builders.append(
            BuilderConfig(
                name = module + ' all',
                slavenames = ['localslave'], # whatever it is a meta builder
                category = module,
                factory = QtMetaBuildstepsFactory(module)
            )
        )

    for conf in configs:
        qtFactory = QtBuildstepsFactory(
            platform = configs[conf]['platform'],
            module = configs[conf]['module'],
        )
        builders.append(
            BuilderConfig(
                name = conf,
                slavenames = configs[conf]['slaves'],
                factory = qtFactory,
                # used in the console view to merge several builders into a single category
                category = configs[conf]['category'],
            )
        )

        # updaters create archives of the module they build
        qtUpdaterFactory = QtBuildstepsFactory(
            platform = configs[conf]['platform'],
            module = configs[conf]['module'],
            archive = True,
        )
        builders.append(
            BuilderConfig(
                name = 'update-' + conf,
                slavenames = configs[conf]['slaves'],
                factory = qtUpdaterFactory,
                category = configs[conf]['category'],
            )
        )

    return builders
