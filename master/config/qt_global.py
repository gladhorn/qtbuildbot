

class QtGlobal():
    modules = []
    # List of modules is written in init-repository script, the easiest way is to parse it.
    import urllib2
    # we take dev branch as it is the first one obtaining new modules
    init_repository_script = urllib2.urlopen("https://qt.gitorious.org/qt/qt5/raw/dev:init-repository")

    # find "my @DEFAULT_REPOS = qw(" line
    line = init_repository_script.readline()
    while not line.startswith("my @DEFAULT_REPOS = qw(") and line:
        line = init_repository_script.readline()

    line = init_repository_script.readline()
    while not line.startswith(");") and line:
        module = "qt/" + line.strip()
        modules.append(module)
        line = init_repository_script.readline()

