####### STATUS TARGETS

from buildbot.status import html
from buildbot.status.web import auth, authz
from buildbot.status.mail import MailNotifier

def get_status():
    """ 'status' is a list of Status Targets. The results of each build will be
    pushed to these targets. buildbot/status/*.py has a variety to choose from,
    including web pages, email senders, and IRC bots.

    The access via login to force builds is also configured here.
    """

    status = []
    authz_cfg=authz.Authz(
        # change any of these to True to enable; see the manual for more
        # options
        auth=auth.BasicAuth([("qt", "qt")]), # FIXME maybe we should use a better password ;)
        gracefulShutdown = 'auth',
        forceBuild = 'auth',
        forceAllBuilds = False,
        pingBuilder = False,
        stopBuild = 'auth',
        stopAllBuilds = False,
        cancelPendingBuild = 'auth',
    )
    status.append(html.WebStatus(http_port=8010, authz=authz_cfg))

    #mn = MailNotifier(fromaddr="buildbot@qt-project.org",
                      #sendToInterestedUsers=False, mode='all',
                      #extraRecipients=['ci-something-list@qt-project.org'],
                      #useTls=True, relayhost="smtp.foo.bar", smtpPort=587, smtpUser="buildbot@qt-project.org", smtpPassword="foobar")
    #status.append(mn)
    return status
