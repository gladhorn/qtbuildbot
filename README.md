# Qt Build Bot #

This repository contains an experimental buildbot setup for the Qt project as early warning system.

## Design Goals ##
* reproducible
    * build slaves can be re-created from scratch with a single command
    * each build/test run clones a template VM and destroys it afterwards
    * changes to a machine template must pass CI tests in order to be incorporated
* reliable
    * slaves have a way of measuring their health (disk/cpu/ram)
    * notification on missing slaves
    * measuring of build/test run times and comparing them, alarm when they increase a lot
* fast
    * artifacts are used to track the baseline for each repository
    * when a build completes the tests can be run in parallel by several slaves
* understandable
    * try to keep things simple
    * no layers of scripts on top of each other
    * non-trivial things require documentation so that it will be easy to make modifications in the future
    * every developer should be able to make a change without getting scared (at least not too much)
* flexible
    * adding a new branch in an existing module (qtbase gets the new 6.1 branch) must work and be tested without change to the buildbot setup
    * adding a new module should require at most one line, ideally it also requires no configuration
    * it is trivial to mark tests or individual test functions as "ignored" and there is still a good indication of what failed but was ignored/insignificant
* helpful
    * early feedback - push a change to gerrit and get instant replies when it breaks compilation on any platform
    * the buildbot will be publicly visible
    * it will be possible to submit try runs of a patch for a selected platform
* informative
    * produces output that helps understanding where we have problems and which tests fail

Ideally we use BuildBot 0.8.9 without modifications.
Setting up the entire infra-structure on a new machine should be straight forward.

## Documentation ##
The documentation is build using Sphinx.
All new code must be documented to make it simple to make changes to the system at a later stage.
To build the documentation install python-sphinx and run make html in the doc directory.

## Windows Slaves ##

see http://trac.buildbot.net/wiki/RunningBuildbotOnWindows

* python2.7
* PyWin32
* Twisted
* setuptools (ez_setup.py, needed to be run in PowerShell for me)

* Zope.interface via easy_install
* buildbot using easy_install

You should now have c:\python27\scripts\buildslave

### Currently qtbase 5.4 has these 'platform targets': ###
    'linux-android-g++_Ubuntu_12.04_x64',
    'linux-android_armeabi-g++_Ubuntu_12.04_x64',
    'linux-arm-gnueabi-g++_Ubuntu_11.10_x86',
    'linux-g++_developer-build_OpenSuSE_13.1_x64',
    'linux-g++_developer-build_qtnamespace_qtlibinfix_RHEL65_x64',
    'linux-g++_developer-build_qtnamespace_qtlibinfix_Ubuntu_11.10_x64',
    'linux-g++_no-widgets_Ubuntu_12.04_x64',
    'linux-g++_shadow-build_Ubuntu_11.10_x86',
    'linux-imx6-armv7a_Ubuntu_12.04_x64',
    'linux-qnx-armv7le_Ubuntu_12.04_x64',
    'linux-qnx-x86_Ubuntu_12.04_x64',
    'macx-clang_developer-build_OSX_10.8',
    'macx-clang_developer-build_OSX_10.9',
    'macx-clang_developer-build_qtnamespace_OSX_10.7',
    'macx-clang_no-framework_OSX_10.8',
    'macx-clang_no-framework_OSX_10.9',
    'macx-ios-clang_OSX_10.9',
    'revdep-qtdeclarative_linux-g++_developer-build_qtnamespace_qtlibinfix_Ubuntu_11.10_x64',
    'revdep-qtdeclarative_linux-g++_shadow-build_Ubuntu_11.10_x86',
    'win32-mingw47_developer-build_qtlibinfix_Windows_7',
    'win32-mingw48_developer-build_qtlibinfix_opengl_Windows_7',
    'win32-msvc2010_Windows_7',
    'win32-msvc2010_developer-build_angle_Windows_7',
    'win32-msvc2010_developer-build_qtnamespace_Windows_7',
    'win64-msvc2012_developer-build_qtnamespace_Windows_81',
    'win64-msvc2013_developer-build_qtnamespace_Windows_81',
    'wince70embedded-armv4i-msvc2008_Windows_7',
    'winrt-x64-msvc2013_Windows_81',
